from flask import Flask
from werkzeug.contrib.fixers import ProxyFix
app = Flask(__name__)


@app.route('/')
def hello():
    return "<h1>Post Commit test first AWS app!!!++</h1>"


@app.route('/about')
def about():
    return '<h1>The about page</h1>'

app.wsgi_app = ProxyFix(app.wsgi_app)
if __name__ == '__main__':
    app.run()
